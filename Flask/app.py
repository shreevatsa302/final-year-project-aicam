from flask import Flask, render_template, Response, jsonify
import cv2
from flask_socketio import SocketIO, emit, join_room, leave_room
from io import StringIO
import io

class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)

    def __del__(self):
        self.video.release()        

    def get_frame(self):
        ret, frame = self.video.read()

        # DO WHAT YOU WANT WITH TENSORFLOW / KERAS AND OPENCV

        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()

class StreamCamera(object):
    def __init__(self):
        self.video1 = cv2.VideoCapture('http://192.168.15.61:8080/video')

    def __del__(self):
        self.video1.release()        

    def get_frame(self):
        ret, frame = self.video1.read()

        # DO WHAT YOU WANT WITH TENSORFLOW / KERAS AND OPENCV

        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()
     
     
app = Flask(__name__)

video_stream = VideoCamera()

stream_cam = StreamCamera()

socketio = SocketIO(app, always_connect=True, engineio_logger=False)


@app.route('/')
def index():
    return render_template('index.html')

def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@socketio.on('image')
def image(data_image):
    sbuf = StringIO()
    sbuf.write(data_image)

    b = io.BytesIO(base64.b64decode(data_image))
    pimg = Image.open(b)

    frame = cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)

    frame = imutils.resize(frame, width=700)
    frame = cv2.flip(frame, 1)
    imgencode = cv2.imencode('.jpg', frame)[1]

    # base64 encode
    stringData = base64.b64encode(imgencode).decode('utf-8')
    b64_src = 'data:image/jpg;base64,'
    stringData = b64_src + stringData

    # emit the frame back
    emit('response_back', stringData)
    
    
@app.route('/video_feed')
def video_feed():
   return Response(gen(video_stream),
            mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/video1')
def video1():
   return Response(gen(stream_cam),
            mimetype='multipart/x-mixed-replace; boundary=frame')
   
   
if __name__ == '__main__':
    app.run(host='192.168.15.62',port="5000")