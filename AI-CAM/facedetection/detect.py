import cv2
import time
import re
import time


def face_detection(ip,keys):
    temp = ip
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

    from main import museumRef
    while True:
        try:
            print('playing',ip)
            if 'youtube.com/' in ip or 'youtu.be/' in ip:  # if source is YouTube video
                import pafy
                ip = pafy.new(ip).getbest(preftype="mp4").url  # YouTube URL
            ip = eval(ip) if ip.isnumeric() else ip
            cap = cv2.VideoCapture(ip)
            

            if not cap.isOpened():
                raise NameError('Just a Dummy Exception, write your own')
            
            while cap.isOpened():
                # Extract the frame
                try:
                    print("Face detection runing")
                    cap.set(cv2.CAP_PROP_POS_FRAMES, cv2.CAP_PROP_FRAME_COUNT-1)

                    ret, img = cap.read()
                    if not ret: continue

                    flag = 0
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
                    for (x,y,w,h) in faces:
                        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
                        roi_gray = gray[y:y+h, x:x+w]
                        roi_color = img[y:y+h, x:x+w]
                        eyes = eye_cascade.detectMultiScale(roi_gray)
                        for (ex,ey,ew,eh) in eyes:
                            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
                            print("People are staring at the camera")
                            flag = 1
                    
                    json = {}
                    json['ip'] = temp
                    json['status'] = 'ON'
                    json['time']= int(time.time())

                    if(flag ==0):
                        json['detected'] = False
                    else : 
                        json['detected'] = True
                    museumRef.child(keys).set(json)
                except cv2.error as error:
                    print("[Error]: {}".format(error))
                    json = {}
                    json['ip'] = temp
                    json['status'] = 'OFF'
                    json['detected'] = False
                    json['time']= int(time.time())

                    museumRef.child(keys).set(json)
                except Exception as e:
                    print("Exception:", e)
                    json = {}
                    json['ip'] = temp
                    json['status'] = 'OFF'
                    json['detected'] = False
                    json['time']= int(time.time())

                    museumRef.child(keys).set(json)
                except :
                    json = {}
                    json['ip'] = temp
                    json['status'] = 'OFF'
                    json['detected'] = False
                    json['time']= int(time.time())

                    museumRef.child(keys).set(json)
                    
        except cv2.error as error:
            print("[Error]: {}".format(error))
            json = {}
            json['ip'] = temp
            json['status'] = 'OFF'
            json['detected'] = False
            json['time']= int(time.time())

            museumRef.child(keys).set(json)
        except Exception as e:
            print("Exception:", e)
            json = {}
            json['ip'] = temp
            json['status'] = 'OFF'
            json['detected'] = False
            json['time']= int(time.time())

            museumRef.child(keys).set(json)
        except :
            json = {}
            json['ip'] = temp
            json['status'] = 'OFF'
            json['detected'] = False
            json['time']= int(time.time())
            museumRef.child(keys).set(json)
