import random
import multiprocessing
from time import sleep
import yolov5
import cv2
import torch
import re
import time
import numpy as np

from threading import Thread
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import db
from functions import class_room_module
from server import update_server
from facedetection.detect import face_detection
from parking.detect import parking_detection
if not firebase_admin._apps:
  cred = credentials.Certificate("key.json")
  firebase_admin.initialize_app(cred,{'databaseURL': "https://yolov5-48c42-default-rtdb.firebaseio.com"})

database = db
ref = db.reference("Classroom")
museumRef = db.reference("Museum")
parkingRef = db.reference("Parking")

serverRef = db.reference("server_status")

jobs = []
ips = []

             

def deleteCam(data):
    global jobs
    global ips
    i = ips.index(data)
    process = jobs[i]
    process.terminate()
    print('terminated')
    jobs.pop(i)
    ips.pop(i)
    print(jobs)
    print(ips)


def processStart(data):
    global jobs,ips,model,ref

    for keys in data:
        print(keys)
        ip = data[keys]['ip']
        module = data[keys]['module']
        process2 = None
        print(ip,module)
        if(module!=None and module=="classroom"):
            process = multiprocessing.Process(target=class_room_module, args=(model,ip,keys))
        elif(module!=None and module=="museum"):
            process = multiprocessing.Process(target=face_detection, args=(ip,keys))
        
        elif(module!=None and module=="parking"):
            ip2 = data[keys]['ip2']
            process = multiprocessing.Process(target=parking_detection,args=(ip,keys,True))
            process2 = multiprocessing.Process(target=parking_detection,args=(ip2,keys,False))
        
        ips.append(keys)
        jobs.append(process)
        if process2!=None:
            jobs.append(process2)
    
    for j in jobs:
        j.start()

def processAdd(data,key):
    global jobs,ips,model,ref
    ip = data['ip']
    module = data['module']
    process2 = None
    print(ip,module)
    ips.append(key)
    if(module!=None and module=="classroom"):
        process = multiprocessing.Process(target=class_room_module, args=(model,ip,key))
    elif(module!=None and module=="museum"):
        process = multiprocessing.Process(target=face_detection, args=(ip,key))
    elif(module!=None and module=="parking"):
        ip2 = data['ip2']
        process = multiprocessing.Process(target=parking_detection,args=(ip,key,True))
        process2 = multiprocessing.Process(target=parking_detection,args=(ip2,key,False))
    
    jobs.append(process)
    if process2!=None:
        jobs.append(process2)
        process2.start()
    process.start()
    
    
def listener(event):

 
    ar = str(event.path).count("/")
    if(event.path=="/"):
      if(event.data!=None):
        processStart(event.data)
      else:
        print('no data') 
    elif(ar==2):
        print("update")
    else:
        if event.data==None:
            s = event.path.replace("/","")
            deleteCam(s)
        else:
            s = event.path.replace("/","")
            processAdd(event.data,s)
                
    
    
if __name__ == "__main__":
    print('Loading')
    model = yolov5.load('yolov5s.pt')
    print('Model Loaded')
    print("Updating server")
    p =multiprocessing.Process(target=update_server)
    p.start()
    db.reference('camera').listen(listener)
    

