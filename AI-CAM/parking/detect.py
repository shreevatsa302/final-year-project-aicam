import cv2
import time
import re
import time
from random import randint
import numpy as np


def parking_detection(ip,keys,entry):
    temp = ip
    from main import parkingRef,database
    while True:
        try:
            print('playing',ip)
            if 'youtube.com/' in ip or 'youtu.be/' in ip:  # if source is YouTube video
                import pafy
                ip = pafy.new(ip).getbest(preftype="mp4").url  # YouTube URL
            ip = eval(ip) if ip.isnumeric() else ip
            cap = cv2.VideoCapture(ip)
            
            fgbg=cv2.createBackgroundSubtractorMOG2(detectShadows=False,history=200,varThreshold = 90)
            kernalOp = np.ones((3,3),np.uint8)
            kernalCl = np.ones((11,11),np.uint8)
            cars = []
            max_p_age = 20
            pid = 1
            cnt_up=0
            cnt_down=0


            line_up=400
            line_down=250

            up_limit=230
            down_limit=int(4.5*(500/5))
            if not cap.isOpened():
                raise NameError('Just a Dummy Exception, write your own')
            
            while cap.isOpened():
                try:
                    #cap.set(cv2.CAP_PROP_POS_FRAMES, cv2.CAP_PROP_FRAME_COUNT-1)

                    ret, frame = cap.read()
                    if not ret: continue
                    
                    frame=cv2.resize(frame,(900,500))
                    for i in cars:
                        i.age_one()
                    fgmask=fgbg.apply(frame)

                    ret,imBin=cv2.threshold(fgmask,200,255,cv2.THRESH_BINARY)
                    mask = cv2.morphologyEx(imBin, cv2.MORPH_OPEN, kernalOp)
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernalCl)


                    (countours0,hierarchy)=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
                    for cnt in countours0:
                        area=cv2.contourArea(cnt)
                        if area>300:

                            m=cv2.moments(cnt)
                            cx=int(m['m10']/m['m00'])
                            cy=int(m['m01']/m['m00'])
                            x,y,w,h=cv2.boundingRect(cnt)


                            new=True
                            if cy in range(up_limit,down_limit):
                                for i in cars:
                                    if abs(x - i.getX()) <= w and abs(y - i.getY()) <= h:
                                        new = False
                                        i.updateCoords(cx, cy)

                                        if i.going_UP(line_down,line_up)==True:
                                            cnt_up+=1

                                        elif i.going_DOWN(line_down,line_up)==True:
                                            cnt_down+=1

                                        break
                                    if i.getState()=='1':
                                        if i.getDir()=='down'and i.getY()>down_limit:
                                            i.setDone()
                                        elif i.getDir()=='up'and i.getY()<up_limit:
                                            i.setDone()
                                    if i.timedOut():
                                        index=cars.index(i)
                                        cars.pop(index)
                                        del i

                                if new==True:
                                    p=Car(pid,cx,cy,max_p_age)
                                    cars.append(p)
                                    pid+1
                            cv2.circle(frame, (cx, cy), 2, (0, 0, 255), -1)









                
                    json = {}
                     
                    if entry:
                        json['ip'] = temp
                    else : 
                        json['ip2'] = temp

                    json['status'] = 'ON'
                    json['time']= int(time.time())
                    if entry:
                        json['entered'] = cnt_down
                    else:
                        json['exited'] = cnt_up

                    parkingRef.child(keys).update(json)

                    # def increment_entrycars(current_value):
                    #     return cnt_up if current_value else 1
                    # def increment_exitcars(current_value):
                    #     return cnt_down if current_value else 1

                    # upcount = database.reference('Parking/'+keys+"/entred")
                    # downcount = database.reference('Parking/'+keys+"/exited")

                    # try:
                    #     if entry and cnt_up>0:
                    #         upcount.transaction(increment_entrycars)
                    #     elif not entry and cnt_down>0:
                    #         downcount.transaction(increment_exitcars)
                    #     print('Transaction completed',cnt_up,cnt_down,ip)
                    # except database.TransactionAbortedError:
                    #         print('Transaction failed to commit')


                except cv2.error as error:
                    print("[Error]: {}".format(error))
                    json = {}
                    if entry:
                        json['ip'] = temp
                    else : 
                        json['ip2'] = temp
                    json['status'] = 'OFF'
                    json['time']= int(time.time())

                    parkingRef.child(keys).update(json)
                except Exception as e:
                    print("Exception:", e)
                    json = {}
                    if entry:
                        json['ip'] = temp
                    else : 
                        json['ip2'] = temp
                    json['status'] = 'OFF'
                    json['time']= int(time.time())

                    parkingRef.child(keys).update(json)
                except :
                    json = {}
                    if entry:
                        json['ip'] = temp
                    else : 
                        json['ip2'] = temp
                    json['status'] = 'OFF'
                    json['time']= int(time.time())

                    parkingRef.child(keys).update(json)
                    
        except cv2.error as error:
            print("[Error]: {}".format(error))
            json = {}
            if entry:
                json['ip'] = temp
            else : 
                json['ip2'] = temp
            json['status'] = 'OFF'
            json['time']= int(time.time())

            parkingRef.child(keys).update(json)
        except Exception as e:
            print("Exception:", e)
            json = {}
            if entry:
                json['ip'] = temp
            else : 
                json['ip2'] = temp
            json['status'] = 'OFF'
            json['time']= int(time.time())

            parkingRef.child(keys).update(json)
        except :
            json = {}
            if entry:
                json['ip'] = temp
            else : 
                json['ip2'] = temp
            json['status'] = 'OFF'
            json['time']= int(time.time())
            parkingRef.child(keys).update(json)



class Car:
    tracks=[]
    def __init__(self,i,xi,yi,max_age):
        self.i=i
        self.x=xi
        self.y=yi
        self.tracks=[]
        self.done=False
        self.state='0'
        self.age=0
        self.max_age=max_age
        self.dir=None

    def getTracks(self):
        return self.tracks

    def getId(self): #For the ID
        return self.i

    def getState(self):
        return self.state

    def getDir(self):
        return self.dir

    def getX(self):  #for x coordinate
        return self.x

    def getY(self):  #for y coordinate
        return self.y

    def updateCoords(self, xn, yn):
        self.age = 0
        self.tracks.append([self.x, self.y])
        self.x = xn
        self.y = yn

    def setDone(self):
        self.done = True

    def timedOut(self):
        return self.done

    def going_UP(self, mid_start, mid_end):
        if len(self.tracks)>=2:
            if self.state=='0':
                if self.tracks[-1][1]<mid_end and self.tracks[-2][1]>=mid_end:
                    state='1'
                    self.dir='up'
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False

    def going_DOWN(self,mid_start,mid_end):
        if len(self.tracks)>=2:
            if self.state=='0':
                if self.tracks[-1][1]>mid_start and self.tracks[-2][1]<=mid_start:
                    start='1'
                    self.dir='down'
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False

    def age_one(self):
        self.age+=1
        if self.age>self.max_age:
            self.done=True
        return  True

#Class2

class MultiCar:
    def __init__(self,cars,xi,yi):
        self.cars=cars
        self.x=xi
        self.y=yi
        self.tracks=[]
        self.done=False
