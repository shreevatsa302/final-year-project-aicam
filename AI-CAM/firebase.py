import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import db
from time import sleep
import multiprocessing 
from multi import listAppend

cred = credentials.Certificate("key.json")
firebase_admin.initialize_app(cred,{'databaseURL': "https://yolov5-48c42-default-rtdb.firebaseio.com"})
ref = db.reference("hello")

jobs = []
ips = []


def deleteCam(data):
    global jobs
    global ips
    i = ips.index(data)
    process = jobs[i]
    process.terminate()
    print('terminated')
    jobs.pop(i)
    ips.pop(i)
    print(jobs)
    print(ips)


def processStart(data):
    global jobs
    global ips
    for keys in data:
        print(keys)
        ip = data[keys]['ip']
        print(ip)
        ips.append(keys)
        process = multiprocessing.Process(target=listAppend, args=(ip,5))
        jobs.append(process)
    
    for j in jobs:
        j.start()
    # for j in jobs:
    #     j.join()


def processAdd(data,key):
    global jobs
    global ips
    ip = data['ip']
    print(ip)
    ips.append(key)
    process = multiprocessing.Process(target=listAppend, args=(ip,5))
    jobs.append(process)
    process.start()
    # for j in jobs:
    #     j.join()
    
def listener(event):
 
    ar = str(event.path).count("/")
    if(event.path=="/"):
        print("First")
        print(type(event.data))
        print(event.data)
        processStart(event.data)
        
        
        
    elif(ar==2):
        print("update")
    else:
        if event.data==None:
            print("deleted")
            s = event.path.replace("/","")
            deleteCam(s)
        else:
            print("new")
            print(event.data)
            s = event.path.replace("/","")
            processAdd(event.data,s)
            
    print(event.event_type)
    print(event.path)
    print(event.data)



    
if __name__ =="__main__":
    db.reference('cameras').listen(listener)

