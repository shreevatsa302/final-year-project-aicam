import cv2
import time
import re
import time

def class_room_module(model,ip,keys):
    temp = ip
    from main import ref
    while True:
        try:
            print('playing',ip)
            if 'youtube.com/' in ip or 'youtu.be/' in ip:  # if source is YouTube video
                import pafy
                ip = pafy.new(ip).getbest(preftype="mp4").url  # YouTube URL
            ip = eval(ip) if ip.isnumeric() else ip
            cap = cv2.VideoCapture(ip)
            

            if not cap.isOpened():
                raise NameError('Just a Dummy Exception, write your own')
                
            count = 0
            while cap.isOpened():
                # Extract the frame
                try:
                    
                    cap.set(cv2.CAP_PROP_POS_FRAMES, cv2.CAP_PROP_FRAME_COUNT-1)
                    ret, frame = cap.read()
                    if not ret: continue
                    
                    name = re.sub('[^A-Za-z0-9]+', '', temp)
                    cv2.imwrite("temp_image/%s.jpg"%name, frame)
                    count = count + 1
                    results = model("temp_image/%s.jpg"%name,size=640,augment=True)
                    print(results.toJson())
                    json = results.toJson()
                    json['ip'] = temp
                    json['status'] = 'ON'
                    json['time']= int(time.time())
                    ref.child(keys).set(json)
                except cv2.error as error:
                    print("[Error]: {}".format(error))
                    json = {}
                    json['ip'] = temp
                    json['status'] = 'OFF'
                    json['detected'] = False
                    json['time']= int(time.time())

                    ref.child(keys).set(json)
                except Exception as e:
                    print("Exception:", e)
                    json = {}
                    json['ip'] = temp
                    json['status'] = 'OFF'
                    json['detected'] = False
                    json['time']= int(time.time())

                    ref.child(keys).set(json)
                except :
                    json = {}
                    json['ip'] = temp
                    json['status'] = 'OFF'
                    json['detected'] = False
                    json['time']= int(time.time())

                    ref.child(keys).set(json)
                    
        except cv2.error as error:
            print("[Error]: {}".format(error))
            json = {}
            json['ip'] = temp
            json['status'] = 'OFF'
            json['detected'] = False
            json['time']= int(time.time())

            ref.child(keys).set(json)
        except Exception as e:
            print("Exception:", e)
            json = {}
            json['ip'] = temp
            json['status'] = 'OFF'
            json['detected'] = False
            json['time']= int(time.time())

            ref.child(keys).set(json)
        except :
            json = {}
            json['ip'] = temp
            json['status'] = 'OFF'
            json['detected'] = False
            json['time']= int(time.time())
            ref.child(keys).set(json)
