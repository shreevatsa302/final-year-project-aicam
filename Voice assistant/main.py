import speech_recognition as sr
import playsound
import os
from gtts import gTTS
r = sr.Recognizer()

def callmirco():
    with sr.Microphone() as source:
        print('Say something')
        audio = r.listen(source)
        try:
            voice_data = r.recognize_google(audio)
            print(voice_data)
        except sr.UnknownValueError:
            print('sorry i did not get that')
        except sr.RequestError:
            print('speech failed')
        return voice_data

def speak(audio_string):
    tts = gTTS(text=audio_string, lang='en')
    audio = 'audio.mp3'
    tts.save(audio)
    playsound.playsound(audio)
    os.remove(audio)
    
    
def replyvoice(data):
    if 'hello' in data:
        print('Hello there') 
        speak('hii, vatsa')
    if 'exit' in data:
        exit()
        

print('Main')
data = callmirco()
replyvoice(data)


from functools import reduce

num=[0,0]
num = [int(n) for n in input().split()]
d = num[0]
p = num[1]


def getprime(lower,upper):
    primelist = []
    for num in range(lower,upper+1):
        if num>1:
            for i in range(2,num):
                if(num%i)==0:
                    primelist.append(0)
                    break
            else:
                primelist.append(num)
        else:
            primelist.append(0)
    return primelist

if d%p==0 and p>1 and d>9 and p<50:
    parts = int(d/p)
    count =1
    setofprime = []
    for i in range(p):
        print(count,parts*(i+1))
        prime = getprime(count,(i+1)*parts)
        setofprime.append(prime)
        count+=parts
    print(reduce(set.intersection,map(set,setofprime))
    
