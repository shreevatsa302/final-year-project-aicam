import cv2
import numpy as np
import speech_recognition as sr
import playsound
import os
from gtts import gTTS
from multiprocessing import Process
import time

r = sr.Recognizer()

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')


def callmirco():
    with sr.Microphone() as source:
        print('Say something')
        audio = r.listen(source)
        try:
            voice_data = r.recognize_google(audio)
            print(voice_data)
        except sr.UnknownValueError:
            print('sorry i did not get that')
        except sr.RequestError:
            print('speech failed')
        replyvoice(voice_data)

def speak():
    tts = gTTS(text='Hey vatsa', lang='en')
    audio = 'audio.mp3'
    tts.save(audio)
    playsound.playsound(audio)
    os.remove(audio)
    
    
def replyvoice(data):
    if 'hello' in data:
        print('Hello there') 
        speak('hii, vatsa')
    if 'exit' in data:
        exit()
        

def streamvideo():
    cap = cv2.VideoCapture(0)
    while(True):
        ret, img = cap.read()
        face = 'no'
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        for (x,y,w,h) in faces:
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            eyes = eye_cascade.detectMultiScale(roi_gray)
            face = 'yes'
            for (ex,ey,ew,eh) in eyes:
                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
                
        cv2.imshow('img',img)
        #if 'yes' in  face:
         #   speak()
          #  time.sleep(20)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()  
    


if __name__=='__main__':
    streamvideo()


