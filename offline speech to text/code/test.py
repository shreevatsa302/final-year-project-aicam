import speech_recognition as sr
import os.path as p

# use the audio file as the audio source
r = sr.Recognizer()
with sr.Microphone() as source:
    print('Say something')
    audio = r.listen(source)
    
print(p.dirname(sr.__file__))
# recognize speech using Sphinx
try:
    print("Sphinx thinks you said " + r.recognize_ibm(audio))
except sr.UnknownValueError:
    print("Sphinx could not understand audio")
except sr.RequestError as e:
    print("Sphinx error; {0}".format(e))